/*
 * Copyright (c) 2018 Yasuo Tabei
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published bytes 
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include <iostream>
#include <cmath>
#include <vector>
#include <string>
#include <fstream>
#include <sstream>

#include <cassert>

#include <time.h>
#include <sys/time.h>

#include "Util.hpp"



class GAkernel {
private:
  void readFile(std::istream &is);
  void initializeKernelMat();
  float computeDTWkernel(const std::vector<uint8_t> &input1, const std::vector<uint8_t> &input2);
  void computeDTWkernelMat();
  float computeScore(uint8_t ch1, uint8_t ch2);
  void initializeDPmat();
  void avoidDiagonalDominant();
  void copyUpperHalf();
  void convertFeatures2IfIdf();
  void outputFile(std::ostream &ofs);
public:
  double getMem() {
    double sum = 0.0;
    sum += sizeof(matchscore_);
    sum += sizeof(mismatchscore_);
    
    for (size_t i = 0; i < inputs_.size(); ++i)
      sum += sizeof(uint8_t) * inputs_[i].size();

    sum += sizeof(float) * kernelMat_.size() * kernelMat_.size();
    sum += sizeof(float);
    return sum;
  }
  
  void run(const char *input_file_name, const char *output_file_name, uint64_t width, float beta, bool avoidDominant);
private:
  float matchscore_;
  float mismatchscore_;
  float gapscore_;
  std::vector<std::vector<uint8_t> > inputs_;
  std::vector<int> labels;
  std::vector<std::vector<float> > kernelMat_;
  std::vector<std::vector<float> > DPmat_;
  uint64_t width_;
  float beta_;
  uint64_t maxLen_;
};
