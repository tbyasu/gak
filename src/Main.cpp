/*
 * Copyright (c) 2018 Yasuo Tabei
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published bytes 
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <iostream>
#include <string>
#include <stdint.h>
#include <time.h>
#include <sys/time.h>
#include <stdint.h>

#include "GAkernel.hpp"
#include "cmdline.h"

using namespace std;

int main(int argc, char **argv) {
  cmdline::parser p;
  p.add<string>("input_file",  'i', "input file name",  true);
  p.add<string>("output_file", 'o', "output file name", true);
  p.add<uint64_t>("width", 'w', "width", false, 0xffffffffffffffff);
  p.add<float>("beta", 'b', "beta", false, 1.0);
  p.add<bool>("avoid_dominant", 'a', "avoid diagonal dominant issus (default: true)", false, true);
  
  p.parse_check(argc, argv);
  
  const string   input_file     = p.get<string>("input_file");
  const string   output_file    = p.get<string>("output_file");
  const uint64_t width          = p.get<uint64_t>("width");
  const float    beta           = p.get<float>("beta");
  const bool     avoid_dominant = p.get<bool>("avoid_dominant");

  GAkernel gak;
  gak.run(input_file.c_str(), output_file.c_str(), width, beta, avoid_dominant);
  
  return 0;
}
