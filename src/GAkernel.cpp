/*
 * Copyright (c) 2018 Yasuo Tabei
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published bytes 
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "GAkernel.hpp"

using namespace std;

double gettimeofday_sec()
{
  struct timeval tv;
  gettimeofday(&tv, NULL);
  return tv.tv_sec + (double)tv.tv_usec*1e-6;
}

void GAkernel::readFile(istream &is) {
  maxLen_ = 0;
  string line;
  while (getline(is, line)) {
    inputs_.resize(inputs_.size() + 1);
    vector<uint8_t> &input = inputs_[inputs_.size() - 1];
    stringstream ss(line);
    int label;
    ss >> label;
    labels.push_back(label);
    
    uint8_t ch;
    while(ss >> ch) 
      input.push_back(ch);

    input.swap(input);
    if (maxLen_ < input.size())
      maxLen_ = input.size();
  }
}

void GAkernel::initializeKernelMat() {
  kernelMat_.resize(inputs_.size());
  for (size_t i = 0; i < kernelMat_.size(); ++i) 
    kernelMat_[i].resize(inputs_.size());
}

void GAkernel::initializeDPmat() {
  DPmat_.resize(maxLen_ + 1);
  for (size_t i = 0; i <= maxLen_; ++i)
    DPmat_[i].resize(maxLen_ + 1);

  DPmat_[0][0] = 0.0; // log(1.f)
  for (size_t i = 1; i <= maxLen_; ++i)
    DPmat_[i][0] = gapscore_ + DPmat_[i-1][0];
  for (size_t i = 1; i <= maxLen_; ++i)
    DPmat_[0][i] = gapscore_ + DPmat_[0][i-1];
}

float GAkernel::computeScore(uint8_t ch1, uint8_t ch2) {
  if (ch1 == ch2)
    return matchscore_;
  return mismatchscore_;
}

float GAkernel::computeDTWkernel(const vector<uint8_t> &input1, const vector<uint8_t> &input2) {
  size_t size1 = input1.size();
  size_t size2 = input2.size();
  
  for (size_t i = 1; i <= size1; ++i) {
    vector<float> &DPvec = DPmat_[i];
    //    size_t upto = min(size2, i + width_);
    //    for (size_t j = max((int64_t)1, (int64_t)i - (int64_t)width_); j <= upto; ++j) {
    for (size_t j = 1; j <= size2; ++j) {
      float sum = DPmat_[i-1][j-1] + computeScore(input1[i-1], input2[j-1]);
      sum = Fast_LogAdd(DPmat_[i-1][j] + gapscore_, sum);
      sum = Fast_LogAdd(DPvec[j-1] + gapscore_, sum);
      DPvec[j] = sum;
      //      DPvec[j] = Fast_LogAdd(0.0, sum);
      /*
      float sum = Fast_LogAdd(DPmat_[i-1][j], DPvec[j-1]);
      sum = Fast_LogAdd(sum, DPmat_[i-1][j-1]);
      sum = Fast_LogAdd(sum, 0.f);
      DPvec[j] = sum + computeScore(input1[i-1], input2[j-1]);
      */
    }
  }

  return DPmat_[size1][size2];
}

void GAkernel::computeDTWkernelMat() {
  for (size_t i = 0; i < inputs_.size(); ++i) {
    cerr << "i: " << i << " " << inputs_.size() << endl;
    vector<uint8_t> &input1 = inputs_[i];
    for (size_t j = i; j < inputs_.size(); ++j) {
      vector<uint8_t> &input2 = inputs_[j];
      kernelMat_[i][j] = computeDTWkernel(input1, input2);
    }
  }
}

void GAkernel::avoidDiagonalDominant() {
  for (size_t i = 0; i < kernelMat_.size(); ++i) {
    for (size_t j = i; j < kernelMat_.size(); ++j) 
      //      kernelMat_[i][j] = log(kernelMat_[i][j])/beta_;
      kernelMat_[i][j] = kernelMat_[i][j]/beta_;
  }
}

void GAkernel::copyUpperHalf() {
  for (size_t i = 0; i < kernelMat_.size(); ++i) {
    for (size_t j = i; j < kernelMat_.size(); ++j) 
      kernelMat_[j][i] = kernelMat_[i][j];
  }
}

void GAkernel::outputFile(ostream &ofs) {
  for (size_t i = 0; i < kernelMat_.size(); ++i) {
    ofs << labels[i] << " " << 0 << ":" << (i+1);
    for (size_t j = 0; j < kernelMat_.size(); ++j) {
      ofs << " " << (j+1) << ":" << kernelMat_[i][j];
    }
    ofs << endl;
  } 
}

void GAkernel::run(const char *input_file_name, const char *output_file_name, uint64_t width, float beta, bool avoidDominant) {
  beta_ = beta;
  //  width_ = width >> 1;

  matchscore_ = MATCHSCORE*beta_; // log(exp(MATCHSCORE*beta_))
  mismatchscore_ = MISMATCHSCORE*beta_; // log(exp(MISMATCHSCORE*beta_))
  gapscore_ = GAPSCORE*beta_; // log(exp(GAPSCORE*beta_))
  
  {
    ifstream ifs(input_file_name);
    if (!ifs) {
      cerr << "cannot open: " << input_file_name << endl;
      exit(1);
    }
    readFile(ifs);
    ifs.close();
  }
  double stime =  gettimeofday_sec();
  initializeKernelMat();

  initializeDPmat();

  computeDTWkernelMat();

  //  if (avoidDominant) 
  avoidDiagonalDominant();

  copyUpperHalf();

  double etime =  gettimeofday_sec();
  
  cout << "cpu time (sec): " << (etime - stime) << endl;
  cout << "memory (mega byte): " << getMem()/1024.0/1024.0 << endl;
  
  {
    ofstream ofs(output_file_name);
    
    if (!ofs) {
      cerr << "cannot open: " << output_file_name << endl;
      exit(1);
    }
    outputFile(ofs);
    ofs.close();
  }
  
}


